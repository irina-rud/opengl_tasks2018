//
// Created by avk on 14.03.18.
//

#ifndef STUDENTTASKS2017_CREATEMAZE_H
#define STUDENTTASKS2017_CREATEMAZE_H

#include <memory>
#include "../src/Maze.h"

std::shared_ptr<Maze> createTestMaze();

#endif //STUDENTTASKS2017_CREATEMAZE_H
