//
// Created by avk on 15.03.18.
//

#include "Maze.h"

void Maze::addWall(Point beg, Point end) {
    _walls.emplace_back(beg, end);
}

void Maze::addWall(Wall wall) {
    _walls.emplace_back(wall);
}

void Maze::addWalls(const std::vector<Wall> &walls) {
    _walls.insert(_walls.end(), walls.begin(), walls.end());
}

const std::vector<Wall> &Maze::getWalls() const {
    return _walls;
}

std::size_t Maze::width() { return _xSize; }

std::size_t Maze::height() { return _ySize; }

bool between(float v, float a, float b) {
    return v <= std::max(a,b) && v >= std::min(a,b);
}
