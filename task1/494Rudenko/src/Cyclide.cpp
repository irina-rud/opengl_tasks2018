//
// Created by Irina Rudenko on 12.03.18.
// you can see the shape on http://virtualmathmuseum.org/Surface/cyclide/cyclide.html
//

#include "Cyclide.h"

MeshPtr CCyclideSurface::make(unsigned int angleShapesNumber) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    int N = angleShapesNumber;

    for (unsigned int i = 0; i <= N; i++) {
        float theta = 2 * (float) glm::pi<float>() * i / N;
        float theta1 = 2 * (float) glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j <= N; j++) {
            float phi = 2 * (float) glm::pi<float>() * j / N;
            float phi1 = 2 * (float) glm::pi<float>() * (j + 1) / N;
            vertices.push_back(countVertex(phi, theta));
            vertices.push_back(countVertex(phi, theta1));
            vertices.push_back(countVertex(phi1, theta));

            normals.push_back(countNormal(phi, theta));
            normals.push_back(countNormal(phi, theta1));
            normals.push_back(countNormal(phi1, theta));

            vertices.push_back(countVertex(phi1, theta1));
            vertices.push_back(countVertex(phi, theta1));
            vertices.push_back(countVertex(phi1, theta));

            normals.push_back(countNormal(phi1, theta1));
            normals.push_back(countNormal(phi, theta1));
            normals.push_back(countNormal(phi1, theta));
        }
    }

    DataBufferPtr bufVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr bufNormals = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufNormals->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufVertices);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, bufNormals);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << vertices.size() << " vertices\n";

    return mesh;
}


glm::vec3 CCyclideSurface::countVertex(double u, double v) {
    double x = (d * (c - a * cos(u) * cos(v)) + b * b * cos(u)) / (a - c * cos(u) * cos(v));
    double y = b * sin(u) * (a - d * cos(v)) / (a - c * cos(u) * cos(v));
    double z = b * sin(v) * (c * cos(u) - d) / (a - c * cos(u) * cos(v));
    return glm::vec3({x, y, z});
}

glm::vec3 CCyclideSurface::countNormal(double u, double v) {
    double x_u = (sin(u) * (d * (a - c) * (a + c) * cos(v) - a * b * b)) / pow((a - c * cos(u) * cos(v)), 2);
    double x_v = (cos(u) * sin(v) * (a * a * d - b * b * c * cos(u) - c * c * d)) / pow((a - c * cos(u) * cos(v)), 2);

    double y_u = (b * (a - d * cos(v)) * (a * cos(u) - c * cos(v))) / pow((a - c * cos(u) * cos(v)), 2);
    double y_v = (a * b * sin(u) * sin(v) * (d - c * cos(u))) / pow((a - c * cos(u) * cos(v)), 2);

    double z_u = (b * c * sin(u) * sin(v) * (d * cos(v) - a)) / pow((a - c * cos(u) * cos(v)), 2);
    double z_v = (b * (d - c * cos(u)) * (c * cos(u) - a * cos(v))) / pow((a - c * cos(u) * cos(v)), 2);
    glm::vec3 dev_u = glm::vec3(x_u, y_u, z_u);
    glm::vec3 dev_v = glm::vec3(x_v, y_v, z_v);
    return glm::normalize(glm::cross(dev_u, dev_v));
}

