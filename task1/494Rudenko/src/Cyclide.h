//
// Created by Irina Rudenko on 12.03.18.
// you can see the shape on http://virtualmathmuseum.org/Surface/cyclide/cyclide.html
//

#ifndef STUDENTTASKS2017_CYCLIDE_H
#define STUDENTTASKS2017_CYCLIDE_H


#include <Application.hpp>
#include <cmath>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>

class CCyclideSurface {
public:

    CCyclideSurface(float _a, float _b, float _c, float _d)
            : a(_a), b(_b), c(_c), d(_d) {}

    CCyclideSurface()
            : a(5), b(3), c(4), d(4.5) {}

    MeshPtr make(unsigned int angleShapesNumber);

    glm::vec3 countVertex(double u, double v);

    glm::vec3 countNormal(double u, double v);

    float a;
    float b;
    float c;
    float d;
};

#endif //STUDENTTASKS2017_CYCLIDE_H
