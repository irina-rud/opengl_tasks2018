//
// Created by Irina Rudenko on 12.03.18.
//

#include "Cyclide.h"

std::string data = "./494RudenkoData/";

class SampleApplication : public Application {
public:
    MeshPtr _surface_mesh;
    CCyclideSurface _surface_obj;

    ShaderProgramPtr _shader;

    void makeScene() override {
        Application::makeScene();

        _shader = std::make_shared<ShaderProgram>();
        _shader->createProgram("./494RudenkoData/shaderNormal.vert", "./494RudenkoData/shader.frag");

        _surface_obj = CCyclideSurface(a, b, c, d);

        _surface_mesh = _surface_obj.make((unsigned int) angleShapesNumber);
    }

    void handleKey(int key, int scancode, int action, int mods) {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS) {
                updateShapes(-1);
                makeScene();
            }
            if (key == GLFW_KEY_EQUAL) {
                updateShapes(+1);
                makeScene();
            }
        }
    }

    void draw() override {

        Application::draw();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер
        _shader->use();
        _surface_mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(
                                        glm::inverse(glm::mat3(_camera.viewMatrix * _surface_mesh->modelMatrix()))));

        //Рисуем меш
        _shader->setMat4Uniform("modelMatrix", _surface_mesh->modelMatrix());
        _surface_mesh->draw();
    }

    SampleApplication(float _a, float _b, float _c, float _d, unsigned int angleShapesNumber,
                      unsigned int pressDelta = 1) :
            a(_a), b(_b), c(_c), d(_d), angleShapesNumber(angleShapesNumber), delta(pressDelta) {}

    SampleApplication() : a(0.52), b(0.5), angleShapesNumber(40), delta(1.0f) {
        c = sqrt(a * a - b * b);
        d = c * 1.2 + 0.01;
    }

private:
    float a;
    float b;
    float c;
    float d;
    float angleShapesNumber;
    float delta;
    const int MIN_SHAPES = 4;
    const int MAX_SHAPES = 1000;


    void updateShapes(int sign) {

        angleShapesNumber += delta * sign;

        if (angleShapesNumber < MIN_SHAPES) {
            angleShapesNumber = MIN_SHAPES;
        } else if (angleShapesNumber > MAX_SHAPES) {
            angleShapesNumber = MAX_SHAPES;
        }
    }

};

int main() {
    SampleApplication application;
    application.start();
    return 0;
}