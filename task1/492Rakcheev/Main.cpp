#include "TreeApplication.h"


int main()
{
    TreeApplication app;
    std::map<char, std::string> rules;
    // rules['F'] = [1.0, "F[-F]<[F]>[+F][F]"];
    // rules['F'] = [1.0, "F[-F]F[+F][F]"];
    // rules['A'] = std::vector(std::pair<0.8, "[+FBF]">, std::pair<0.2, "F">);
    // rules['B'] = std::vector(std::pair<0.6, "[-FBF]">, std::pair<0.4, "F">);
    rules['A'] = "[F[+FCA][-FCA]]";
    rules['B'] = "[F[>FCB][<FCB]]";
    rules['C'] = "[F[^FCA][&FCA]]";
    std::string initialString = "ABC";
    float rotateAngle = 35;
    float stepDistance = 0.3;
    float distanceScale = 0.8;
    float angleScale = 1;
    float radiusScale = 0.8;
    float initialBottomRadius = 0.06;
    uint16_t numIterations = 5;

    app.setupLSystem(
        rules,
        initialString,
        rotateAngle,
        stepDistance,
        distanceScale,
        angleScale,
        radiusScale,
        initialBottomRadius,
        numIterations
    );
    app.start();

    return 0;
}
