#include <Application.hpp>
#include "Mesh.hpp"
#include <ShaderProgram.hpp>

#include <iostream>
#include <utility>
#include <vector>
#include <Common.h>

#include "LSystem.h"


class TreeApplication : public Application {
    std::vector<MeshPtr> branches;
    ShaderProgramPtr shader;
    LSystem system;
    std::vector<float> points;
public:
    void setupLSystem(
        std::map<char, std::string> rules,
        std::string initialString,
        float rotateAngle,
        float stepDistance,
        float distanceScale,
        float angleScale,
        float radiusScale,
        float initialBottomRadius,
        uint16_t numIterations
    );
    MeshPtr makeBranch(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomRadius, float topRadius);
    void makeScene() override;
    void draw() override;
};
