#include "Application.hpp"

#include <iostream>
#include <vector>

// Набор коллбеков для обработки различных событий

// События клавиатуры
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    auto *app = (Application *) glfwGetWindowUserPointer(window);
    app->handleKey(key, scancode, action, mods);
}

// Изменение размера окна
void windowSizeChangedCallback(GLFWwindow *window, int width, int height) {
}

// Нажатие на кнопку мыши
void mouseButtonPressedCallback(GLFWwindow *window, int button, int action, int mods) {
}

// Изменение позиции курсора
void mouseCursorPosCallback(GLFWwindow *window, double xpos, double ypos) {
    auto *app = (Application *) glfwGetWindowUserPointer(window);
    app->handleMouseMove(xpos, ypos);
}

// Событие скролла
void scrollCallback(GLFWwindow *window, double xoffset, double yoffset) {
    auto *app = (Application *) glfwGetWindowUserPointer(window);
    app->handleScroll(xoffset, yoffset);
}

// class Application

Application::~Application() {
    ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();
}

void Application::start() {
    initContext();
    initGL();
    initGUI();
    makeScene();
    run();
}

void Application::initContext() {
    if (!glfwInit()) {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window_ = glfwCreateWindow(1920, 1080, "Zufar Latypov 493 - OpenGL - Task_1", nullptr, nullptr);

    if (!window_) {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    glfwMakeContextCurrent(window_);

    // Вертикальная синхронизация
    glfwSwapInterval(1);

    // Регистрируем указатель на данный объект, чтобы потом использовать его в функциях обратного вызова}
    glfwSetWindowUserPointer(window_, this);

    // Связываем наши callback-и
    glfwSetKeyCallback(window_, keyCallback);
    glfwSetWindowSizeCallback(window_, windowSizeChangedCallback);
    glfwSetMouseButtonCallback(window_, mouseButtonPressedCallback);
    glfwSetCursorPosCallback(window_, mouseCursorPosCallback);
    glfwSetScrollCallback(window_, scrollCallback);
}

void Application::initGL() {
    glewExperimental = GL_TRUE;
    glewInit();
    // Получаем имя рендерера
    const GLubyte *renderer = glGetString(GL_RENDERER);
    // Получаем номер версии
    const GLubyte *version = glGetString(GL_VERSION);
    std::cout << "Renderer: " << renderer << std::endl;
    std::cout << "OpenGL version supported: " << version << std::endl;
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void Application::makeScene() {

}

void Application::run() {
    while (!glfwWindowShouldClose(window_)) //Пока окно не закрыто
    {
        // Проверяем события ввода
        glfwPollEvents();
        // Обновляем сцену и положение виртуальной камеры
        update();
        updateGUI();
        // Рисуем один кадр
        draw();
        drawGUI();
        // Переключаем передний и задний буферы
        glfwSwapBuffers(window_);
    }
}

void Application::handleKey(int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_ESCAPE) {
            glfwSetWindowShouldClose(window_, GL_TRUE);
        }
    }
    camera_handler_->handleKey(window_, key, scancode, action, mods);
}

void Application::handleMouseMove(double xpos, double ypos) {
    if (ImGui::IsMouseHoveringAnyWindow()) {
        return;
    }

    camera_handler_->handleMouseMove(window_, xpos, ypos);
}

void Application::handleScroll(double x_offset, double y_offset) {
    camera_handler_->handleScroll(window_, x_offset, y_offset);
}

void Application::update() {
    double dt = glfwGetTime() - old_time_;
    old_time_ = glfwGetTime();
    camera_handler_->update(window_, dt);
    camera_ = camera_handler_->GetCameraInfo();
}

void Application::draw() {
}

void Application::initGUI() {
    ImGui_ImplGlfwGL3_Init(window_, true);
}

void Application::updateGUI() {
    ImGui_ImplGlfwGL3_NewFrame();
}

void Application::drawGUI() {
    ImGui::Render();
}