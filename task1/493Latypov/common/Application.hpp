#pragma once

#include "Camera.hpp"

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw_gl3.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

class Application {
public:
    Application() = default;

    ~Application();

    // Запускает приложение
    void start();

    //Обрабатывает нажатия кнопок на клавитуре. См. сигнатуру GLFWkeyfun библиотеки GLFW
    virtual void handleKey(int key, int scancode, int action, int mods);

    //Обрабатывает движение мышки
    virtual void handleMouseMove(double xpos, double ypos);

    // Обрабатывает колесико мыши
    virtual void handleScroll(double x_offset, double y_offset);

protected:
    // Инициализирует графический контекст
    virtual void initContext();

    // Настраивает некоторые параметры OpenGL
    virtual void initGL();

    // Инициализирует графический интерфейс пользователя
    virtual void initGUI();

    // Создает трехмерную сцену
    virtual void makeScene();

    // Запускает цикл рендеринга
    void run();

    // Выполняет обновление сцены и виртуальной камеры
    virtual void update();

    // Выполняет обновление графического интерфейса пользователя
    virtual void updateGUI();

    // Отрисовывает один кадр
    virtual void draw();

    // Отрисовывает графический интерфейс пользователя
    virtual void drawGUI();

    // Графичеcкое окно
    GLFWwindow *window_ = nullptr;

    CameraInfo camera_;
    std::shared_ptr<CameraHandler> camera_handler_;

    // Время на предыдущем кадре
    double old_time_ = 0.0;
};
