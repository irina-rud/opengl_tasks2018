set(SRC_FILES
    common/Application.cpp
        common/DebugOutput.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    Cone.cpp
    Main.cpp
    TreeApplication.cpp
    LSystem.cpp
)

set(HEADER_FILES
    TreeApplication.h
    LSystem.h
    Cone.h
    common/Application.hpp
        common/DebugOutput.h
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

include_directories(common)

MAKE_TASK(492Zabelin 1 "${SRC_FILES}")
