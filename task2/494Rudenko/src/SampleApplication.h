//
// Created by Irina Rudenko on 08.04.18.
//
#ifndef STUDENTTASKS2017_SAMPLEAPPLICATION_H
#define STUDENTTASKS2017_SAMPLEAPPLICATION_H

#include "Cyclide.h"
#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <iostream>

class SampleApplication : public Application {
public:
    std::string data_folder = "./494RudenkoData/";

    MeshPtr _surface_mesh;
    MeshPtr _marker;

    ShaderProgramPtr _shaderPerFragment;
    ShaderProgramPtr _markerShader;

    CCyclideSurface _surface_obj;

    LightInfo _light;
    TexturePtr _worldTexture;
    GLuint _sampler;

    float _lr = 5.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    void makeScene() override;

    void updateGUI() override;

    void handleKey(int key, int scancode, int action, int mods) override;

    void draw() override;

    SampleApplication(float _a, float _b, float _c, float _d, unsigned int angleShapesNumber,
                      unsigned int pressDelta = 1);

    SampleApplication();

private:
    float a;
    float b;
    float c;
    float d;
    float angleShapesNumber;
    float delta;
    const int MIN_SHAPES = 4;
    const int MAX_SHAPES = 1000;
    double _period = 15;

    void updateShapes(int sign);

};

#endif //STUDENTTASKS2017_SAMPLEAPPLICATION_H
