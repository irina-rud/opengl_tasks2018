//
// Created by Irina Rudenko on 08.04.18.
//
#include "SampleApplication.h"

void SampleApplication::makeScene() {
    Application::makeScene();

    _marker = makeSphere(0.1f);

    _shaderPerFragment = std::make_shared<ShaderProgram>(data_folder + "texture.vert", data_folder + "texture.frag");

    _markerShader = std::make_shared<ShaderProgram>(data_folder + "marker.vert", data_folder + "marker.frag");

    _worldTexture = loadTexture("494RudenkoData/texture.jpeg");

    _surface_obj = CCyclideSurface(a, b, c, d);

    _surface_mesh = _surface_obj.make((unsigned int) angleShapesNumber);

    _light.position =
            glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    _light.ambient = glm::vec3(0.2, 0.2, 0.2);
    _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
    _light.specular = glm::vec3(1.0, 1.0, 1.0);

    glGenSamplers(1, &_sampler);
    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void SampleApplication::handleKey(int key, int scancode, int action, int mods) {
    Application::handleKey(key, scancode, action, mods);
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_MINUS) {
            updateShapes(-1);
            makeScene();
        }
        if (key == GLFW_KEY_EQUAL) {
            updateShapes(+1);
            makeScene();
        }
    }
}

void SampleApplication::draw() {
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::vec3 lightPos =
            glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;

    _shaderPerFragment->use();

//    _shaderPerFragment->setFloatUniform("time", static_cast<float>(glfwGetTime()));

    _shaderPerFragment->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shaderPerFragment->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _light.position =
            glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

    _shaderPerFragment->setVec3Uniform("light.pos", lightPosCamSpace);
    _shaderPerFragment->setVec3Uniform("light.La", _light.ambient);
    _shaderPerFragment->setVec3Uniform("light.Ld", _light.diffuse);
    _shaderPerFragment->setVec3Uniform("light.Ls", _light.specular);

    glActiveTexture(GL_TEXTURE0);
    glBindSampler(0, _sampler);
    _worldTexture->bind();
    _shaderPerFragment->setIntUniform("diffuseTex", 0);


    {
        _shaderPerFragment->setMat4Uniform("modelMatrix", _surface_mesh->modelMatrix());
        _shaderPerFragment->setMat3Uniform("normalToCameraMatrix",
                                           glm::transpose(
                                                   glm::inverse(
                                                           glm::mat3(
                                                                   _camera.viewMatrix *
                                                                   _surface_mesh->modelMatrix()))));
        using namespace std::chrono;
        milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        float phase = (sin(ms.count() / 1000.0) + 1) / 2;
        _shaderPerFragment->setFloatUniform("phase", phase);

        _surface_mesh->draw();
    }

    {
        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                   glm::translate(glm::mat4(1.0f), lightPos));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();
    }
    glBindSampler(0, 0);
    glUseProgram(0);

}

void SampleApplication::updateGUI() {
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light")) {
            ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

            ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
            ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
        }
    }
    ImGui::End();
}

SampleApplication::SampleApplication(float _a, float _b, float _c, float _d, unsigned int angleShapesNumber,
                                     unsigned int pressDelta) :
        a(_a), b(_b), c(_c), d(_d), angleShapesNumber(angleShapesNumber), delta(pressDelta) {}

SampleApplication::SampleApplication() : a(2.7), b(2.5), angleShapesNumber(100), delta(1.0f) {
    c = sqrt(a * a - b * b);
    d = a * 0.5;
}

void SampleApplication::updateShapes(int sign) {

    angleShapesNumber += delta * sign;

    if (angleShapesNumber < MIN_SHAPES) {
        angleShapesNumber = MIN_SHAPES;
    } else if (angleShapesNumber > MAX_SHAPES) {
        angleShapesNumber = MAX_SHAPES;
    }
}
