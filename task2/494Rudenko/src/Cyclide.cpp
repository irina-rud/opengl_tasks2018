//
// Created by Irina Rudenko on 12.03.18.
// you can see the shape on http://virtualmathmuseum.org/Surface/cyclide/cyclide.html
//

#include "Cyclide.h"

MeshPtr CCyclideSurface::make(unsigned int angleShapesNumber) {
    std::vector<glm::vec3> vertices_1;
    std::vector<glm::vec3> vertices_2;
    std::vector<glm::vec3> normals_1;
    std::vector<glm::vec3> normals_2;
    std::vector<glm::vec2> texcoords;

    int N = angleShapesNumber;

    for (unsigned int i = 0; i <= N; i++) {
        float theta = 2 * (float) glm::pi<float>() * i / N;
        float theta1 = 2 * (float) glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j <= N; j++) {
            float phi = 2 * (float) glm::pi<float>() * j / N;
            float phi1 = 2 * (float) glm::pi<float>() * (j + 1) / N;
            vertices_1.push_back(countVertex(phi, theta));
            vertices_1.push_back(countVertex(phi1, theta));
            vertices_1.push_back(countVertex(phi, theta1));

            normals_1.push_back(countNormal(phi, theta));
            normals_1.push_back(countNormal(phi1, theta));
            normals_1.push_back(countNormal(phi, theta1));

            vertices_1.push_back(countVertex(phi1, theta1));
            vertices_1.push_back(countVertex(phi, theta1));
            vertices_1.push_back(countVertex(phi1, theta));

            normals_1.push_back(countNormal(phi1, theta1));
            normals_1.push_back(countNormal(phi, theta1));
            normals_1.push_back(countNormal(phi1, theta));

            texcoords.push_back(glm::vec2((float) j / N, (float) i / N));
            texcoords.push_back(glm::vec2((float) (j + 1) / N, (float) i / N));
            texcoords.push_back(glm::vec2((float) j / N, (float) (i + 1) / N));

            texcoords.push_back(glm::vec2((float) (j + 1) / N, (float) (i + 1) / N));
            texcoords.push_back(glm::vec2((float) j / N, (float) (i + 1) / N));
            texcoords.push_back(glm::vec2((float) (j + 1) / N, (float) i / N));

        }
    }
    b = a;
    c = 0;
    d = a * 0.5;

    for (unsigned int i = 0; i <= N; i++) {
        float theta = 2 * (float) glm::pi<float>() * i / N;
        float theta1 = 2 * (float) glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j <= N; j++) {
            float phi = 2 * (float) glm::pi<float>() * j / N;
            float phi1 = 2 * (float) glm::pi<float>() * (j + 1) / N;
            vertices_2.push_back(countVertex(phi, theta));
            vertices_2.push_back(countVertex(phi1, theta));
            vertices_2.push_back(countVertex(phi, theta1));

            normals_2.push_back(countNormal(phi, theta));
            normals_2.push_back(countNormal(phi1, theta));
            normals_2.push_back(countNormal(phi, theta1));

            vertices_2.push_back(countVertex(phi1, theta1));
            vertices_2.push_back(countVertex(phi, theta1));
            vertices_2.push_back(countVertex(phi1, theta));

            normals_2.push_back(countNormal(phi1, theta1));
            normals_2.push_back(countNormal(phi, theta1));
            normals_2.push_back(countNormal(phi1, theta));


        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices_1.size() * sizeof(float) * 3, vertices_1.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals_1.size() * sizeof(float) * 3, normals_1.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(vertices_2.size() * sizeof(float) * 3, vertices_2.data());

    DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf4->setData(normals_2.size() * sizeof(float) * 3, normals_2.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices_1.size());

    return mesh;
}

glm::vec3 CCyclideSurface::countVertex(double u, double v) {
    double x = (d * (c - a * cos(u) * cos(v)) + b * b * cos(u)) / (a - c * cos(u) * cos(v));
    double y = b * sin(u) * (a - d * cos(v)) / (a - c * cos(u) * cos(v));
    double z = b * sin(v) * (c * cos(u) - d) / (a - c * cos(u) * cos(v));
    return glm::vec3({x, y, z});
}

glm::vec3 CCyclideSurface::countNormal(double u, double v) {
    double x_u = (sin(u) * (d * (a - c) * (a + c) * cos(v) - a * b * b)) / pow((a - c * cos(u) * cos(v)), 2);
    double x_v = (cos(u) * sin(v) * (a * a * d - b * b * c * cos(u) - c * c * d)) / pow((a - c * cos(u) * cos(v)), 2);

    double y_u = (b * (a - d * cos(v)) * (a * cos(u) - c * cos(v))) / pow((a - c * cos(u) * cos(v)), 2);
    double y_v = (a * b * sin(u) * sin(v) * (d - c * cos(u))) / pow((a - c * cos(u) * cos(v)), 2);

    double z_u = (b * c * sin(u) * sin(v) * (d * cos(v) - a)) / pow((a - c * cos(u) * cos(v)), 2);
    double z_v = (b * (d - c * cos(u)) * (c * cos(u) - a * cos(v))) / pow((a - c * cos(u) * cos(v)), 2);
    glm::vec3 dev_u = glm::vec3(x_u, y_u, z_u);
    glm::vec3 dev_v = glm::vec3(x_v, y_v, z_v);
    return glm::normalize(glm::cross(dev_u, dev_v));
}

