//
// Created by Irina Rudenko on 12.03.18.
//

#include "SampleApplication.h"


int main() {
    SampleApplication application;
    application.start();
    return 0;
}