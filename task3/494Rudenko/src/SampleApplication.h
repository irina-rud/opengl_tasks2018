//
// Created by Irina Rudenko on 08.04.18.
//
#ifndef STUDENTTASKS2017_SAMPLEAPPLICATION_H
#define STUDENTTASKS2017_SAMPLEAPPLICATION_H

#include "Cyclide.h"
#include <Application.hpp>
#include <LightInfo.hpp>
#include <Framebuffer.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <sstream>


class SampleApplication : public Application
{
public:
    MeshPtr _surface_mesh;

    MeshPtr _quad;

    ShaderProgramPtr _quadColorShader;
    ShaderProgramPtr _renderDeferredShader;
    ShaderProgramPtr _explosionShader;

    float _lr;
    float _phi;
    float _theta;

    float _lightIntensity = 1.0;
    LightInfo _light;
    CameraInfo _lightCamera;

    TexturePtr _earthTex;

    GLuint _sampler;
    GLuint _repeatSampler;
    GLuint _depthSampler;

    FramebufferPtr _gbufferFB;
    TexturePtr _depthTex;
    TexturePtr _normalsTex;
    TexturePtr _diffuseTex;

    FramebufferPtr _deferredFB;
    TexturePtr _deferredTex;

    int _oldWidth = 1024;
    int _oldHeight = 1024;

    SampleApplication();

private:
    float angleShapesNumber = 30;
    float shapesdelta = 0.1;

    float speed = 0.005f;
    float defaultspeed = 0.005f;
    float shift=0.0;
    const float shiftspeed = 0.0001;
    const int MIN_SHAPES = 4;
    const int MAX_SHAPES = 100;
    const float MIN_SHIFT = 0;
    const float MAX_SHIFT = 10;

    bool explosion_mode=false;
    bool run_explosion = false;

    void initFramebuffers();
    void makeScene() override;
    void updateGUI() override;
    void handleKey(int key, int scancode, int action, int mods) override;
    void update() override;
    void draw() override;
    void drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera);
    void drawDeferred(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera);
    void drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture);
    void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera);
    void updateShapes(int sign);
};

#endif //STUDENTTASKS2017_SAMPLEAPPLICATION_H
