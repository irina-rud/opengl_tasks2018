////
//// Created by Irina Rudenko on 08.04.18.
////
#include "SampleApplication.h"


const std::string common = "494RudenkoData/";

SampleApplication::SampleApplication() :
        Application() {}

void SampleApplication::initFramebuffers()
{
    _gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

    _normalsTex = _gbufferFB->addBuffer(GL_RGB16F, GL_COLOR_ATTACHMENT0);
    _diffuseTex = _gbufferFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT1);
    _depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

    _gbufferFB->initDrawBuffers();

    if (!_gbufferFB->valid())
    {
        std::cerr << "Failed to setup framebuffer\n";
        exit(1);
    }

    _deferredFB = std::make_shared<Framebuffer>(1024, 1024);
    _deferredTex = _deferredFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);
    _deferredFB->initDrawBuffers();

    if (!_deferredFB->valid())
    {
        std::cerr << "Failed to setup framebuffer\n";
        exit(1);
    }
}

void SampleApplication::makeScene()
{
    Application::makeScene();

    float a=2.7;
    float b = 2.5;
    float c = sqrt(a * a - b * b);
    float d = a * 0.5;

    _surface_mesh = CCyclideSurface(a, b, c, d).make((unsigned int) angleShapesNumber);
    _surface_mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

    _explosionShader = std::make_shared<ShaderProgram>();

    ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
    vs->createFromFile(common+"shader.vert");
    _explosionShader->attachShader(vs);

    ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
    gs->createFromFile(common+"shader.geom");
    _explosionShader->attachShader(gs);

    ShaderPtr fs = std::make_shared<Shader>(GL_FRAGMENT_SHADER);
    fs->createFromFile(common+"shader.frag");
    _explosionShader->attachShader(fs);

    _explosionShader->linkProgram();


    _quad = makeScreenAlignedQuad();

    _quadColorShader = std::make_shared<ShaderProgram>();
    _quadColorShader->createProgram(common+"quadColor.vert", common+"quadColor.frag");


    _renderDeferredShader = std::make_shared<ShaderProgram>();
    _renderDeferredShader->createProgram(common+"texture.vert", common+"texture.frag");

    _lr = 10.0;
    _phi = 0.0f;
    _theta = 0.48f;

    _lightIntensity = 4.0f;

    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
    _light.ambient = glm::vec3(0.2, 0.2, 0.2);
    _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
    _light.specular = glm::vec3(1.0, 1.0, 1.0);

    _earthTex = loadTexture(common+"texture.jpeg", SRGB::YES); //sRGB
    glGenSamplers(1, &_sampler);
    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glGenSamplers(1, &_repeatSampler);
    glSamplerParameteri(_repeatSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_repeatSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

    GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

    glGenSamplers(1, &_depthSampler);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

    glfwGetFramebufferSize(_window, &_oldWidth, &_oldHeight);
    initFramebuffers();
}

void SampleApplication::updateGUI()
{
    Application::updateGUI();
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("Irina Rudenko, task3", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

            ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
            ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

            ImGui::SliderFloat("intensity", &_lightIntensity, 0.0f, 5.0f);
        }
        ImGui::Text("shapes count: %d", (int)angleShapesNumber);
        ImGui::Text("Tap space for explosion/pause explosion");
        ImGui::Text("Tap ESC for exit");
    }
    ImGui::End();
}

void SampleApplication::handleKey(int key, int scancode, int action, int mods)
{
    Application::handleKey(key, scancode, action, mods);

}

void SampleApplication::update()
{
    if (!explosion_mode) {
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            updateShapes(-1);
        }

        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            updateShapes(1);
        }
        if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
            explosion_mode = true;
        }
    } else {
        if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
            if  (!run_explosion){
                run_explosion = true;
            }
            else{
                run_explosion = false;
            }
        }
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            speed -= shiftspeed;
            if (speed < 0) {
                speed = 0;
            }

        }

        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            speed += shiftspeed;
        }

        if (run_explosion)
            shift += speed;
        if (shift < MIN_SHIFT) {
            shift = MIN_SHIFT;
        }
        if (shift > MAX_SHIFT) {
            shift = 0;
            explosion_mode = false;
            speed = defaultspeed;
        }
    }
    float a=2.7;
    float b = 2.5;
    float c = sqrt(a * a - b * b);
    float d = a * 0.5;
    _surface_mesh = CCyclideSurface(a, b, c, d).make((unsigned int) angleShapesNumber);


    Application::update();

    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
    _lightCamera.viewMatrix = glm::lookAt(_light.position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);


    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    if (width != _oldWidth || height != _oldHeight)
    {
        _gbufferFB->resize(width, height);
        _deferredFB->resize(width, height);

        _oldWidth = width;
        _oldHeight = height;
    }
}

void SampleApplication::draw()
{
    Application::draw();

    drawToGBuffer(_gbufferFB, _explosionShader, _camera);

    drawDeferred(_deferredFB, _renderDeferredShader, _camera, _lightCamera);

    drawToScreen(_quadColorShader, _deferredTex);

}

void SampleApplication::drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera)
{
    fb->bind();

    glViewport(0, 0, fb->width(), fb->height());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->use();

    shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", camera.projMatrix);
    shader->setFloatUniform("theta", (shift));
    shader->setFloatUniform("costheta", cos(shift));
    shader->setFloatUniform("sintheta", sin(shift));

    glActiveTexture(GL_TEXTURE0);
    glBindSampler(0, _repeatSampler);
    _earthTex->bind();
    shader->setIntUniform("diffuseTex", 0);

    drawScene(shader, camera);

    glUseProgram(0);

    fb->unbind();
}

void SampleApplication::drawDeferred(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera)
{
    fb->bind();

    glViewport(0, 0, fb->width(), fb->height());
    glClear(GL_COLOR_BUFFER_BIT);

    shader->use();
    shader->setMat4Uniform("viewMatrixInverse", glm::inverse(camera.viewMatrix));
    shader->setMat4Uniform("projMatrixInverse", glm::inverse(camera.projMatrix));

    glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

    shader->setVec3Uniform("light.pos", lightPosCamSpace);
    shader->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
    shader->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
    shader->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

    shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
    shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

    glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
    shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);

    glActiveTexture(GL_TEXTURE0);
    glBindSampler(0, _sampler);
    _normalsTex->bind();
    shader->setIntUniform("normalsTex", 0);

    glActiveTexture(GL_TEXTURE1);
    glBindSampler(1, _sampler);
    _diffuseTex->bind();
    shader->setIntUniform("diffuseTex", 1);

    glActiveTexture(GL_TEXTURE2);
    glBindSampler(2, _sampler);
    _depthTex->bind();
    shader->setIntUniform("depthTex", 2);

    glActiveTexture(GL_TEXTURE3);
    glBindSampler(3, _depthSampler);
    shader->setIntUniform("shadowTex", 3);

    _quad->draw();

    glUseProgram(0);

    fb->unbind();
}

void SampleApplication::drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
{
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->use();

    glActiveTexture(GL_TEXTURE0);
    glBindSampler(0, _sampler);
    inputTexture->bind();
    shader->setIntUniform("tex", 0);

    glEnable(GL_FRAMEBUFFER_SRGB);

    _quad->draw();

    glDisable(GL_FRAMEBUFFER_SRGB);

    glBindSampler(0, 0);
    glUseProgram(0);
}

void SampleApplication::drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera)
{
    shader->setMat4Uniform("modelMatrix", _surface_mesh->modelMatrix());
    shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _surface_mesh->modelMatrix()))));

    _surface_mesh->draw();

}


void SampleApplication::updateShapes(int sign) {
    angleShapesNumber += shapesdelta * sign;

    if (angleShapesNumber < MIN_SHAPES) {
        angleShapesNumber = MIN_SHAPES;
    } else if (angleShapesNumber > MAX_SHAPES) {
        angleShapesNumber = MAX_SHAPES;
    }
}